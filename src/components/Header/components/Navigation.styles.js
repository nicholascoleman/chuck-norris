import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(() => ({
  link: {
    padding: '0px 10px',
  },
}))

export default useStyles
