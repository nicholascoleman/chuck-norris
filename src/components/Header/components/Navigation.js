import React from 'react'
import { Link } from 'react-router-dom'
import { Typography } from '@material-ui/core'

import useStyles from './Navigation.styles'

const Navigation = () => {
  const classes = useStyles()

  // #routing
  // #styling - className
  return (
    <Typography data-testid="navigation-component" className={classes.root}>
      <Link className={classes.link} to="/">
        Home
      </Link>
      <Link className={classes.link} to="/search">
        Search
      </Link>
      <Link className={classes.link} to="/categories">
        Categories
      </Link>
      <Link className={classes.link} to="/jokes">
        Jokes
      </Link>
    </Typography>
  )
}

export default Navigation
