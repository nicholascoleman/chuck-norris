import React from 'react'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import { render } from '@testing-library/react'

import HeaderContainer from './HeaderContainer'

const setup = () => {
  const history = createMemoryHistory({ initialEntries: ['/'] })

  return render(
    <Router history={history}>
      <HeaderContainer />
    </Router>
  )
}

describe('<HeaderContainer />', () => {
  test('renders snaphot', () => {
    const component = setup()
    expect(component).toMatchSnapshot()
  })

  test('renders component', () => {
    const { getByTestId } = setup()
    expect(getByTestId('header-container')).toBeTruthy()
  })
})
