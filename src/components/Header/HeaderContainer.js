import React from 'react'
import { Grid } from '@material-ui/core'

import { Navigation } from './components'

const HeaderContainer = () => {
  return (
    <Grid data-testid="header-container" container>
      <Grid item>
        <Navigation />
      </Grid>
    </Grid>
  )
}

export default HeaderContainer
