import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import { Provider as JokesProvider } from 'store/JokesContext'

import JokeTable from './JokeTable'

const props = {
  jokes: [
    {
      category: 'animal',
      id: '1',
      savedOn: 'January 1, 2020',
      value: 'dog joke',
    },
    {
      category: 'people',
      id: '2',
      savedOn: 'March 1, 2020',
      value: 'people joke',
    },
  ],
}

const setup = (ownProps = {}) => {
  const renderedProps = { ...props, ...ownProps }
  const component = render(
    <JokesProvider>
      <JokeTable {...renderedProps} />
    </JokesProvider>
  )
  return component
}

afterEach(cleanup)

describe('<JokeTable />', () => {
  test('renders', () => {
    const component = setup()
    expect(component).toMatchSnapshot()
  })
  test('renders Table', async () => {
    const { getByTestId, getAllByTestId, rerender } = render(<JokeTable {...props} />, {
      wrapper: JokesProvider,
    })
    expect(getByTestId('savedOn-header').textContent).toBe('Saved On')
    expect(getByTestId('category-header').textContent).toBe('category')
    expect(getByTestId('value-header').textContent).toBe('Joke')
    expect(getByTestId('January 1, 2020-cell').textContent).toBe('January 1, 2020')
    expect(getByTestId('animal-cell').textContent).toBe('animal')
    expect(getByTestId('dog joke-cell').textContent).toBe('dog joke')

    // test sorting
    fireEvent.click(getByTestId('savedOn-header'))
    rerender(<JokeTable {...props} />)
    let rows = await getAllByTestId('row')
    expect(rows[0].textContent).toBe('January 1, 2020animaldog joke')

    fireEvent.click(getByTestId('savedOn-header'))
    rerender(<JokeTable {...props} />)
    rows = await getAllByTestId('row')
    expect(rows[0].textContent).toBe('March 1, 2020peoplepeople joke')
  })
})
