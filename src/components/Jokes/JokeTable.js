import React from 'react'
import PropTypes from 'prop-types'
import CssBaseline from '@material-ui/core/CssBaseline'
import MaUTable from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { useTable, useSortBy } from 'react-table'

const JokeTable = ({ jokes }) => {
  const columns = React.useMemo(
    () => [
      {
        Header: 'Saved On',
        accessor: 'savedOn',
      },
      {
        Header: 'category',
        accessor: 'category',
      },
      {
        Header: 'Joke',
        accessor: 'value',
      },
    ],
    []
  )

  const { getTableProps, headerGroups, rows, prepareRow } = useTable(
    {
      columns,
      data: jokes,
    },
    useSortBy
  )
  return (
    <div>
      <CssBaseline />
      <MaUTable {...getTableProps()}>
        <TableHead>
          {headerGroups.map(headerGroup => {
            return (
              <TableRow key={0} {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                  <TableCell
                    data-testid={`${column.id}-header`}
                    key={column.id}
                    {...column.getHeaderProps(column.getSortByToggleProps())}>
                    {column.render('Header')}
                    <span>
                      {
                        // eslint-disable-next-line no-nested-ternary
                        column.isSorted ? (column.isSortedDesc ? ' 🔽' : ' 🔼') : ''
                      }
                    </span>
                  </TableCell>
                ))}
              </TableRow>
            )
          })}
        </TableHead>
        <TableBody>
          {rows.map(row => {
            prepareRow(row)
            return (
              <TableRow data-testid="row" key={row.id} {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return (
                    <TableCell data-testid={`${cell.value}-cell`} key={cell.id} {...cell.getCellProps()}>
                      {cell.render('Cell')}
                    </TableCell>
                  )
                })}
              </TableRow>
            )
          })}
        </TableBody>
      </MaUTable>
    </div>
  )
}

JokeTable.propTypes = {
  jokes: PropTypes.arrayOf(
    PropTypes.shape({
      category: PropTypes.string,
      id: PropTypes.string,
      savedOn: PropTypes.string,
      value: PropTypes.string,
    })
  ),
}

JokeTable.defaultProps = {
  jokes: [],
}

export default JokeTable
