import React from 'react'
import { Container, Grid, Typography } from '@material-ui/core'
import useDeepCompareEffect from 'use-deep-compare-effect'
import { Context as JokesContext } from 'store/JokesContext'

import JokeTable from './JokeTable'

const Jokes = () => {
  const { state } = React.useContext(JokesContext)
  const { saved } = state
  const [jokes, setJokes] = React.useState()

  useDeepCompareEffect(() => {
    // #lists/Keys
    setJokes(Object.values(saved))
  }, [saved])

  if (!jokes?.length) {
    return (
      <Grid container item xs={12} style={{ marginTop: 20 }}>
        <Typography variant="h5">Chuck says &quot;Go save some favorite jokes&quot;</Typography>
      </Grid>
    )
  }

  return (
    <Container data-testid="jokes-container">
      <Grid container item xs={12} style={{ marginTop: 20, marginBottom: 20 }}>
        <Typography variant="h5">Favorite Jokes:</Typography>
      </Grid>
      <JokeTable jokes={[...jokes]} />
    </Container>
  )
}

export default Jokes
