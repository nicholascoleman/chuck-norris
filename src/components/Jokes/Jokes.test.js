import React from 'react'
import { render, cleanup, wait } from '@testing-library/react'
import { Provider as JokesProvider } from 'store/JokesContext'

import Jokes from './Jokes'

const setup = () => {
  return render(
    <JokesProvider>
      <Jokes />
    </JokesProvider>
  )
}

afterEach(cleanup)

describe('<Jokes />', () => {
  test('matches snapshot', () => {
    const component = setup()
    expect(component).toMatchSnapshot()
  })
  test('renders', async () => {
    const { container } = render(<Jokes />, { wrapper: JokesProvider })
    expect(container.textContent).toContain('Chuck says')
  })
})
