import React from 'react'
import { useFormik } from 'formik'
import { Button, Container, FormControlLabel, Grid, Switch, TextField, Typography } from '@material-ui/core'
import { Context as AuthContext } from 'store/AuthContext'
import Spinner from 'components/Spinner'
import { userStatus } from 'helpers/constants'

import useStyles from './Login.styles'

// #arrow function
const validate = values => {
  const errors = {}
  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }

  if (!values.password) {
    errors.password = 'Password Required'
  } else if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/i.test(values.password) || values.password.length > 10) {
    errors.password =
      'Password must have at least 1 capital letter, 1 lower case letter, 1 number, and be between 6-10 characters in length'
  }

  return errors
}

// #functional component
const Login = () => {
  // #hooks
  const classes = useStyles()
  // #hooks
  const { state, userLogin } = React.useContext(AuthContext)

  // #hooks
  const [showPassword, setShowPassword] = React.useState(false)
  const [accountEmail, setAccountEmail] = React.useState()

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validate,
    onSubmit: async values => {
      const { email, password } = values
      userLogin({ user: { email, password } })
      setAccountEmail(email)
    },
  })

  const validateForm = () => {
    const validEmail = formik.getFieldProps('email').value.length > 5 && !formik.errors.email
    const validPassword = formik.getFieldProps('password').value.length > 6 && !formik.errors.password

    return !(validEmail && validPassword)
  }

  if (state.authStatus === userStatus.PENDING) {
    return (
      <Grid container justify="center" alignItems="center">
        <Grid item xs={12}>
          <Typography data-testid="signup-spinner-title" align="center" style={{ marginTop: 100 }}>
            Signing Up with {accountEmail} email
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Spinner />
        </Grid>
      </Grid>
    )
  }
  // #styles - className, style, material-uis
  return (
    <Container data-testid="login-component" component="main" maxWidth="xs">
      <Grid container justify="center" className={classes.paper}>
        <Grid item xs={12}>
          <Typography data-testid="login-title" component="h1" variant="h5">
            Chuck Wants You to Log In
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <form className={classes.form} onSubmit={formik.handleSubmit}>
            <TextField
              label="Email"
              name="email"
              id="email"
              variant="outlined"
              margin="normal"
              required
              fullWidth
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.email}
              data-testid="email-textFied"
              inputProps={{
                'data-testid': 'email-input',
              }}
            />
            {formik.touched.email && formik.errors.email ? (
              <div style={{ color: 'red' }}>{formik.errors.email}</div>
            ) : null}
            <TextField
              data-testid="password-textField"
              name="password"
              label="Password"
              id="password"
              variant="outlined"
              margin="normal"
              required
              fullWidth
              type={showPassword ? 'text' : 'password'}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.password}
              inputProps={{
                'data-testid': 'password-input',
              }}
            />
            {formik.touched.password && formik.errors.password ? (
              <div style={{ color: 'red' }}>{formik.errors.password}</div>
            ) : null}
            <FormControlLabel
              data-testid="password-switch"
              value="start"
              control={<Switch color="primary" />}
              label="Show Password"
              labelPlacement="start"
              onChange={() => setShowPassword(!showPassword)}
            />
            <Button
              data-testid="submit-button"
              type="submit"
              disabled={validateForm()}
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}>
              Log In
            </Button>
          </form>
        </Grid>
      </Grid>
      {accountEmail && (
        <Grid item>
          <Typography data-testid="account-email">Account created for: {accountEmail}</Typography>
        </Grid>
      )}
    </Container>
  )
}

export default Login
