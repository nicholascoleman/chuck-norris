import React from 'react'
import { render, cleanup, fireEvent, wait } from '@testing-library/react'
import { Provider as AuthProvider } from 'store/AuthContext'

import Login from './Login'

const setup = () => {
  return render(
    <AuthProvider>
      <Login />
    </AuthProvider>
  )
}

afterEach(cleanup)

describe('<Login />', () => {
  test('matches snapshot ', () => {
    const component = setup()
    expect(component).toMatchSnapshot()
  })

  test('renders title', () => {
    const { getByTestId } = render(<Login />, { wrapper: AuthProvider })
    const title = getByTestId('login-title')
    expect(title.textContent).toBe('Chuck Wants You to Log In')
  })

  test('renders form', async () => {
    const { getByTestId, queryByTestId, rerender } = render(<Login />, {
      wrapper: AuthProvider,
      userLogin: jest.fn(),
    })
    const email = getByTestId('email-input')
    const password = getByTestId('password-input')
    const submitButton = getByTestId('submit-button')

    await wait(() => {
      fireEvent.change(email, { target: { value: 'dog@cat.com' } })
    })

    await wait(() => {
      fireEvent.change(password, { target: { value: 'Password1' } })
    })

    expect(email.value).toEqual('dog@cat.com')
    expect(password.value).toEqual('Password1')
    expect(queryByTestId('account-email')).toBeNull()

    await wait(() => {
      fireEvent.click(submitButton)
    })

    rerender(<Login />)

    expect(getByTestId('signup-spinner-title').textContent).toContain(`Signing Up with dog@cat.com email`)
  })
})
