import React from 'react'
import { Context as JokesContext } from 'store/JokesContext'

import Category from './Category'

const Categories = () => {
  const { saveJoke } = React.useContext(JokesContext)

  const onSaveJoke = (joke, category) => {
    saveJoke(joke, category)
  }

  const ownProps = {
    handleSaveJoke: onSaveJoke,
  }

  // #descruturer
  return <Category {...ownProps} />
}

export default Categories
