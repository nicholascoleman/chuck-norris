import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { Provider as JokesProvider } from 'store/JokesContext'

import Categories from './Categories'

const setup = () => {
  return render(
    <JokesProvider>
      <Categories />
    </JokesProvider>
  )
}

afterEach(cleanup)

describe('<Categories />', () => {
  test('renders', () => {
    const component = setup()
    expect(component).toMatchSnapshot()
  })

  test('renders', () => {
    const { getByTestId } = render(<Categories />, { wrapper: JokesProvider })
    expect(getByTestId('category-title').textContent).toBe('Joke Categories')
  })
})
