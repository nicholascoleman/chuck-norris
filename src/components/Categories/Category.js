import React from 'react'
import PropTypes from 'prop-types'
import { Button, Card, CardActions, CardContent, Container, Grid, Typography } from '@material-ui/core'
import { getCategories, getRandomJoke } from 'store/api'
import Spinner from 'components/Spinner'

// # Class component example - componentDidMount and render lifecycle methods
class Category extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      categories: [],
      category: '',
      saved: false,
    }
  }

  // #Promise
  async componentDidMount() {
    const response = await getCategories()
    this.setState(() => ({ categories: response.data }))
  }

  getJoke = async category => {
    this.setState(() => ({ category }))
    const response = await getRandomJoke(category)
    // #state
    this.setState(() => ({ joke: response.data }))
  }

  saveJoke = () => {
    // #props
    const { handleSaveJoke } = this.props
    const { category, joke, saved } = this.state

    handleSaveJoke(joke, category)
    this.setState({ saved: !saved, joke: '' })
  }

  renderSaveJokeButton(joke) {
    return (
      <CardActions>
        <Button data-testid="favorite-button" onClick={() => this.saveJoke(joke)} size="small">
          Make Favorite
        </Button>
      </CardActions>
    )
  }

  renderJoke() {
    // #this
    const { joke, saved } = this.state

    return (
      <Grid item xs={12}>
        <Card style={{ margin: '20px 0', minheight: 100, width: '80%' }}>
          <CardContent data-testid="pick-category-title" style={{ color: 'blue' }}>
            {joke ? joke.value : 'Pick a category'}
          </CardContent>
          {joke && !saved ? this.renderSaveJokeButton(joke) : <div />}
        </Card>
      </Grid>
    )
  }

  renderCategory(category) {
    if (!category) return <Spinner />
    const dataTestId = `${category}-category-button`
    return (
      <Grid item key={category}>
        <Button data-testid={dataTestId} variant="outlined" onClick={() => this.getJoke(category)}>
          {' '}
          {category}
        </Button>
      </Grid>
    )
  }

  // #render
  render() {
    const { categories } = this.state

    return (
      <Container data-testid="category-component" style={{ display: 'flex', flexDirection: 'column', padding: 20 }}>
        <Grid container align="center">
          <Grid item xs={12}>
            <Typography data-testid="category-title" variant="h2">
              Joke Categories
            </Typography>
          </Grid>
        </Grid>
        <Grid container>{this.renderJoke()}</Grid>
        <Grid container spacing={5}>
          {categories.length === 0 ? (
            <Spinner />
          ) : (
            categories.map(category => {
              return this.renderCategory(category)
            })
          )}
        </Grid>
      </Container>
    )
  }
}

Category.propTypes = {
  handleSaveJoke: PropTypes.func.isRequired,
}

export default Category
