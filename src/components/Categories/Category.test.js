import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import { Provider as JokesProvider } from 'store/JokesContext'
import { getCategories, getRandomJoke } from 'store/api'

import Category from './Category'

  const props = {
    handleSaveJoke: jest.fn(),
  }


afterEach(cleanup)

jest.mock('store/api')

describe('<Category />',  () => {
  test('renders', async () => {
    const dogCategory = 'dog'
    getCategories.mockResolvedValueOnce({
      data: [dogCategory]
    })
    getRandomJoke.mockResolvedValueOnce({
      data: { value: 'cat'}
    })

    const {getByTestId, findByText, getByText, rerender } = render(<Category {...props}/>, {wrapper: JokesProvider})
     // test for Spinner when categories are loading
    expect(getByText('...loading')).toBeTruthy()

    await findByText('dog') // continue after categories have loaded
    const dogButton = getByTestId('dog-category-button')
    expect(dogButton.textContent).toEqual(' dog')
    expect(getByTestId('pick-category-title').textContent).toBe('Pick a category')
    fireEvent.click(dogButton)
    rerender(<Category {...props}/>)

    await findByText(/make favorite/i)
    expect(getByTestId('pick-category-title').textContent).toBe('cat')
    const favoriteButton = getByTestId('favorite-button')
    expect(favoriteButton.textContent).toBe('Make Favorite')
    fireEvent.click(favoriteButton)

    rerender(<Category {...props}/>)
    expect(getByTestId('pick-category-title').textContent).toBe('Pick a category')
  });

})
