import React from 'react'
import PropTypes from 'prop-types'
import { Button, Checkbox, Grid, ListItem, ListItemText } from '@material-ui/core'
import FavoriteIcon from '@material-ui/icons/Favorite'
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder'
import { Context as JokesContext } from 'store/JokesContext'

const SearchResults = ({ joke, handleFlipStringTruncation }) => {
  const { saveJoke, removeJoke } = React.useContext(JokesContext)
  const [checked, setChecked] = React.useState(joke.checked)

  const handleChange = event => {
    const { target } = event
    setChecked(target.checked)
    target.checked ? saveJoke(joke) : removeJoke(joke.id)
  }

  const truncateString = str => {
    if (str < 50) return str
    return `${str.slice(0, 50)} ...`
  }

  // #let
  let str = joke.value
  if (joke.truncated) {
    str = truncateString(joke.value)
  }
  return (
    <ListItem key={joke.id} data-testid="search-joke-component">
      <ListItemText>
        <Grid container>
          <Grid item xs={1}>
            <Checkbox
              data-testid="favorite-checkbox"
              icon={<FavoriteBorderIcon />}
              checkedIcon={<FavoriteIcon />}
              checked={checked}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={11}>
            <Button data-testid="flip-truncate-button" onClick={() => handleFlipStringTruncation(joke)}>
              {str}
            </Button>
          </Grid>
        </Grid>
      </ListItemText>
    </ListItem>
  )
}

SearchResults.propTypes = {
  handleFlipStringTruncation: PropTypes.func.isRequired,
  joke: PropTypes.shape({
    checked: PropTypes.bool,
    id: PropTypes.string.isRequired,
    truncated: PropTypes.bool,
    value: PropTypes.string.isRequired,
  }),
}

SearchResults.defaultProps = {
  joke: {
    checked: false,
  },
}

export default SearchResults
