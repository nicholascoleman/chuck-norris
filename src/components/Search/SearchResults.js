import React from 'react'
import PropTypes from 'prop-types'
import cloneDeep from 'lodash/cloneDeep'
import { Container, Grid, List, Typography } from '@material-ui/core'

import SearchJoke from './SearchJoke'

const SearchResults = ({ jokes, setJokes }) => {
  const flipStringTruncation = joke => {
    const newJokes = cloneDeep(jokes)
    const index = newJokes.findIndex(item => item.id === joke.id)
    newJokes[index].truncated = !newJokes[index].truncated
    setJokes(newJokes)
  }

  if (jokes?.length === 0) return <div data-testid="results-empty" />

  return (
    <Container>
      <Grid container>
        <Grid container item justify="center" xs={1}>
          <Typography data-testid="results-title">Favorite</Typography>
        </Grid>
        <Grid container item justify="center" xs={11}>
          <Typography data-testid="results-subtitle">Joke (click to expand)</Typography>
        </Grid>
      </Grid>
      <List data-testid="results-jokes-list">
        {jokes.map(joke => (
          <SearchJoke key={joke.id} joke={joke} handleFlipStringTruncation={flipStringTruncation} />
        ))}
      </List>
    </Container>
  )
}

SearchResults.propTypes = {
  jokes: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    })
  ).isRequired,
  setJokes: PropTypes.func.isRequired,
}

export default SearchResults
