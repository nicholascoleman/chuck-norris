import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import { Provider as JokesProvider } from 'store/JokesContext'

import SearchJoke from './SearchJoke'

const value = 'Spicy jalapeno bacon ipsum dolor amet sausages beef leberkas sausages'
const truncatedValue = `${value.substring(0, 50)} ...`
const props = {
  handleFlipStringTruncation: jest.fn(),
  joke: {
    checked: false,
    id: '2020',
    truncated: true,
    value,
  },
}

afterEach(() => {
  cleanup()
})

describe('<Search Joke', () => {
  test('renders', () => {
    const component = render(<SearchJoke {...props} />, { wrapper: JokesProvider })
    expect(component).toMatchSnapshot()
  })

  test('changes favorite joke checkbox when clicked', () => {
    const { getByTestId } = render(<SearchJoke {...props} />, { wrapper: JokesProvider })

    const checkbox = getByTestId('favorite-checkbox').querySelector('input[type="checkbox"]')
    expect(checkbox).toHaveProperty('checked', false)
    fireEvent.click(checkbox)
    expect(checkbox).toHaveProperty('checked', true)
    fireEvent.click(checkbox)
    expect(checkbox).toHaveProperty('checked', false)
  })

  test('flips string shown from truncated to full length when joke is clicked', () => {
    const ownProps = { ...props, joke: { ...props.joke, truncated: false } }
    const { getByTestId, rerender } = render(<SearchJoke {...props} />, { wrapper: JokesProvider })
    const truncateButton = getByTestId('flip-truncate-button')
    expect(truncateButton.textContent).toBe(truncatedValue)
    fireEvent.click(truncateButton)
    rerender(<SearchJoke {...ownProps} />)
    expect(props.handleFlipStringTruncation).toHaveBeenCalledWith(props.joke)
    expect(truncateButton.textContent).toBe(value)
  })

  test('shows full joke if is truncated is false', () => {
    const ownProps = { ...props, joke: { ...props.joke, truncated: false } }
    const { getByTestId } = render(<SearchJoke {...ownProps} />, { wrapper: JokesProvider })
    const jokeText = getByTestId('flip-truncate-button')
    expect(jokeText.textContent).toBe(value)
  })
})
