import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(() => ({
  paper: {
    marginTop: 16,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%',
    marginTop: 8,
  },
  submit: {
    margin: '24px 0px 16px 0px',
  },
}))

export default useStyles
