import React from 'react'
import { render, rerender, cleanup, fireEvent } from '@testing-library/react'
import { Provider as JokesProvider } from 'store/JokesContext'

import SearchResults from './SearchResults'

const value = 'Spicy jalapeno bacon ipsum dolor amet sausages beef leberkas sausages'

const props = {
  jokes: [
    {
      checked: false,
      id: '2020',
      truncated: true,
      value,
    },
  ],
  setJokes: jest.fn(),
}

const setup = (ownProps = {}) => {
  const setupProps = { ...props, ...ownProps }
  return render(
    <JokesProvider>
      <SearchResults {...setupProps} />
    </JokesProvider>
  )
}

afterEach(cleanup)

describe('<SearchResult />', () => {
  test('renders', () => {
    const component = setup()
    expect(component).toMatchSnapshot()
  })

  test('renders page header', () => {
    const { getByTestId, queryByTestId } = setup()
    const title = getByTestId('results-title')
    expect(title).toHaveTextContent(/favorite/i)

    const subtitle = getByTestId('results-subtitle')
    expect(subtitle).toHaveTextContent(/joke \(click to expand\)/i)

    expect(queryByTestId('result-empty')).toBeNull()
    const list = getByTestId('results-jokes-list')
    expect(list).not.toBeNull()
  })

  test('renders an empty div if no jokes provided', () => {
    const ownProps = { ...props, jokes: [] }
    const { queryByTestId } = setup(ownProps)
    expect(queryByTestId('results-empty')).not.toBeNull()
  })
})
