import React from 'react'
import { render, cleanup, fireEvent, wait } from '@testing-library/react'
import { Provider as JokesProvider } from 'store/JokesContext'

import Search from './Search'

afterEach(() => {
  cleanup()
})

describe('<Search />', () => {
  test('matches snapshot ', () => {
    const component = render(<Search />, { wrapper: JokesProvider })
    expect(component).toMatchSnapshot()
  })

  test(' renders', async () => {
    const { getByTestId, rerender } = render(<Search />, { wrapper: JokesProvider })
    const title = getByTestId('search-title')
    expect(title.textContent).toContain('Search for Jokes')

    const searchTerm = getByTestId('searchTerm-input')
    await wait(() => {
      fireEvent.change(searchTerm, { target: { value: 'dog ' } })
    })

    expect(searchTerm.value).toEqual('dog ')

    rerender(<Search />)

    const searchButton = getByTestId('search-button')
    await wait(() => {
      fireEvent.click(searchButton)
    })
    rerender(<Search />)

    expect(getByTestId('results-empty')).toBeTruthy()
  })
})
