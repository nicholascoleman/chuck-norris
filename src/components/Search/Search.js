import React from 'react'
import { useFormik } from 'formik'
import { Button, Container, TextField, Typography } from '@material-ui/core'
import { Context as JokesContext } from 'store/JokesContext'

import SearchResults from './SearchResults'
import useStyles from './Search.styles'

const Search = () => {
  const classes = useStyles()
  const { state, searchJokes } = React.useContext(JokesContext)
  const [jokes, setJokes] = React.useState(state.jokes)

  // #event handling
  React.useEffect(() => {
    setJokes(state.jokes)
  }, [state.jokes])

  const formik = useFormik({
    initialValues: {
      searchTerm: '',
    },
    onSubmit: async (values, { resetForm }) => {
      const { searchTerm } = values
      searchJokes(searchTerm)
      resetForm()
    },
  })

  // #jsx
  return (
    <Container data-testid="search-container" component="main" maxWidth="md">
      <div className={classes.paper}>
        <Typography data-testid="search-title">Search for Jokes</Typography>
        <form className={classes.form} onSubmit={formik.handleSubmit}>
          <TextField
            data-testid="searchTerm-textField"
            label="Search Term"
            name="searchTerm"
            id="searchTerm"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            value={formik.values.searchTerm}
            inputProps={{
              'data-testid': 'searchTerm-input',
            }}
          />
          <Button
            data-testid="search-button"
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}>
            Search
          </Button>
        </form>
      </div>
      <SearchResults jokes={jokes} setJokes={setJokes} />
    </Container>
  )
}

export default Search
