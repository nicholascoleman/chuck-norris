import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(() => ({
  root: {
    margin: 20,
  },
}))

export default useStyles
