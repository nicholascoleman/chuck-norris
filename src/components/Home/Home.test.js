import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { Provider as JokesProvider } from 'store/JokesContext'

import Home from './Home'

const setup = () => {
  return render(
    <JokesProvider>
      <Home />
    </JokesProvider>
  )
}

afterEach(cleanup)

describe('<Home />', () => {
  test('renders', () => {
    const component = setup()
    expect(component).toMatchSnapshot()
  })

  test('renders pages descriptions', () => {
    const { getByTestId } = setup()
    expect(getByTestId('home-search')).toHaveTextContent(/searchfind jokes by search term/i)
    expect(getByTestId('home-categories')).toHaveTextContent(/categoriesGet jokes by category/i)
    expect(getByTestId('home-jokes')).toHaveTextContent(/jokessee jokes you selected/i)
  })
})
