import React from 'react'
import { Container, Grid, Typography } from '@material-ui/core'

import useStyles from './Home.styles'

const Home = () => {
  const classes = useStyles()
  return (
    <Container maxWidth="lg" className={classes.root}>
      <Grid container direction="column" justify="space-between">
        <Grid data-testid="home-search" item xs={12}>
          <Typography variant="h3">Search</Typography>
          <Typography variant="subtitle1">Find jokes by Search Term</Typography>
        </Grid>
        <Grid data-testid="home-categories" item xs={12}>
          <Typography variant="h3">Categories</Typography>
          <Typography variant="subtitle1">Get jokes by category</Typography>
        </Grid>
        <Grid data-testid="home-jokes" item xs={12}>
          <Typography variant="h3">Jokes</Typography>
          <Typography variant="subtitle1">See Jokes you selected</Typography>
        </Grid>
      </Grid>
    </Container>
  )
}

export default Home
