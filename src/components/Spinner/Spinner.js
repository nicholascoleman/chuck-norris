import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Card, CardActionArea, CardContent, LinearProgress, Typography } from '@material-ui/core'

const useStyles = makeStyles(() => ({
  root: {
    width: 350,
    marginTop: 100,
  },
  media: {
    height: 200,
  },
}))

export default function Spinner() {
  const classes = useStyles()

  return (
    <Grid data-testid="spinner-component" container item justify="center">
      <Card className={classes.root}>
        <CardActionArea>
          <CardContent>
            <LinearProgress data-testid="linear-progress-component" />
            <Grid container item justify="center">
              <Typography variant="h5">...loading</Typography>
            </Grid>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  )
}
