import React from 'react'
import { render, cleanup } from '@testing-library/react'

import Spinner from './Spinner'

afterEach(() => cleanup())

describe('<Spinner />', () => {
  test('renders', () => {
    const { getByTestId } = render(<Spinner />)
    expect(getByTestId('spinner-component')).toBeTruthy()
  })

  test('renders a loading spinner', () => {
    const { getByText, getByTestId } = render(<Spinner />)
    expect(getByTestId('linear-progress-component')).toBeTruthy()
    expect(getByText(/...loading/i)).toBeTruthy()
  })
})
