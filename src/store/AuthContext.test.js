// import createDataContext from 'store/createDataContext'
import * as types from 'store/types'
import { userStatus } from 'helpers/constants'

import { authReducer, userLogin } from './AuthContext'

// const mockDispatch = jest.spyOn('dispatch', () => jest.fn())

describe('useReducer', () => {
  const initialState = {
    authStatus: userStatus.NOT_AUTHENTICATED,
    error: {},
  }

  test('USER_AUTHENTICATED', async () => {
    const state = initialState
    const action = { type: types.USER_AUTHENTICATED, payload: { email: 'test@test.com' } }
    const response = authReducer(state, action)
    expect(response).toEqual({ authStatus: 'AUTHENTICATED', error: [], email: 'test@test.com' })
  })

  test('USER_NOT_AUTHENTICATED', async () => {
    const state = initialState
    const action = { type: types.NOT_AUTHENTICATED }
    const response = authReducer(state, action)
    expect(response).toEqual({ authStatus: 'NOT_AUTHENTICATED', error: [] })
  })

  test('USER_AUTHENTICATED_ERROR', async () => {
    const state = initialState
    const action = {
      type: types.USER_AUTHENTICATED_ERROR,
      payload: { errorType: 'auth error', error: 'some error' },
    }
    const response = authReducer(state, action)
    expect(response).toEqual({
      authStatus: 'ERROR',
      error: { errorType: 'auth error', error: 'some error' },
    })
  })
})

describe('Auth functions', () => {
  test('userLogin', async () => {
    const user = { email: 'test@test.com', password: 'dog' }
    userLogin({ user })
    // expect(authReducer()).toHaveBeenCalledWith({ action: { type: types.USER_AUTHENTICATED } })
  })
})
