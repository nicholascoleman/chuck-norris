import * as types from 'store/types'

import { jokesReducer, searchJokes, saveJoke, removeJoke, getCategories, getJoke } from './JokesContext'

describe('JokesContext', () => {
  describe('jokesReducer', () => {
    const initialState = {
      categories: [],
      jokes: [],
      saved: {},
      fetchingJokes: false,
      fetchingJoke: false,
      error: {},
    }

    const joke1 = {
      value: 'dog',
      truncated: true,
      checked: false,
      id: '1',
    }
    const joke2 = {
      value: 'cat',
      truncated: true,
      checked: false,
      id: '2',
    }

    describe('SEARCH_JOKES', () => {
      test('no saved jokes', () => {
        const jokes = [joke1]
        const state = initialState
        const action = { type: types.SEARCH_JOKES, payload: { jokes } }
        const response = jokesReducer(state, action)
        expect(response).toEqual({ ...state, jokes, fetchingJokes: false, error: {} })
      })

      test('same saved jokes', () => {
        const jokes = [joke1]
        const state = { ...initialState, saved: { joke1 } }
        const action = { type: types.SEARCH_JOKES, payload: { jokes } }
        const response = jokesReducer(state, action)
        expect(response).toEqual({
          ...state,
          jokes,
          saved: { joke1: { value: 'dog', truncated: true, checked: false, id: '1' } },
          fetchingJokes: false,
          error: {},
        })
      })

      test('different saved jokes', () => {
        const jokes = [joke1]
        const state = { ...initialState, saved: { joke2 } }
        const action = { type: types.SEARCH_JOKES, payload: { jokes } }
        const response = jokesReducer(state, action)
        expect(response).toEqual({
          ...state,
          jokes,
          saved: { joke2: { value: 'cat', truncated: true, checked: false, id: '2' } },
          fetchingJokes: false,
          error: {},
        })
      })
    })

    test('FETCHING_JOKES', () => {
      const state = initialState
      const action = { type: types.FETCHING_JOKES }
      const response = jokesReducer(state, action)
      expect(response.fetchingJokes).toBeTruthy()
    })

    describe('SAVE_JOKE', () => {
      test('no saved jokes', () => {
        const state = initialState
        const joke = { id: joke1.id, value: joke1.value, savedOn: 'Jan 1, 2010 10:20:30', category: 'animal' }
        const action = { type: types.SAVE_JOKE, payload: { [joke.id]: joke } }
        const response = jokesReducer(state, action)
        expect(response.saved).toEqual({ [joke.id]: { ...joke } })
      })
      test('previously saved joke', () => {
        const joke = { id: joke1.id, value: joke1.value, savedOn: 'Jan 1, 2010 10:20:30', category: 'animal' }
        const state = { ...initialState, saved: { [joke.id]: { ...joke } } }
        const action = { type: types.SAVE_JOKE, payload: { [joke.id]: joke } }
        const response = jokesReducer(state, action)
        expect(response.saved).toEqual({ [joke.id]: { ...joke } })
      })
    })

    test('UPDATE_SAVED_JOKES', () => {
      const state = { ...initialState, jokes: [joke1, joke2] }
      const action = { type: types.UPDATE_SAVED_JOKES, payload: { id: joke1.id, checked: true } }
      const response = jokesReducer(state, action)
      const foundJoke = response.jokes.find(joke => joke.id === joke1.id)
      expect(foundJoke.checked).toBeTruthy()
    })

    test('REMOVE_JOKE', () => {
      const state = { ...initialState, saved: { [joke1.id]: joke1, [joke2.id]: joke2 } }
      const action = { type: types.REMOVE_JOKE, payload: joke1.id }
      const response = jokesReducer(state, action)
      expect(response.saved).toEqual({ [joke2.id]: joke2 })
    })

    test('FETCH_JOKE', () => {
      const state = initialState
      const action = { type: types.FETCHING_JOKE }
      const response = jokesReducer(state, action)
      expect(response.fetchingJoke).toBeTruthy
    })

    test('JOKE_ERROR', () => {
      const state = initialState
      const error = 'some error'
      const action = { type: types.JOKE_ERROR, payload: { error } }
      const response = jokesReducer(state, action)
      expect(response.error).toEqual({ error: 'some error' })
    })
  })
})
