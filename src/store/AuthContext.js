/* eslint-disable no-console */
import createDataContext from 'store/createDataContext'
import * as types from 'store/types'
import { userStatus } from 'helpers/constants'

export const initialState = {
  authStatus: userStatus.NOT_AUTHENTICATED,
  error: {},
}

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.USER_AUTHENTICATED:
      return { ...state, error: [], authStatus: userStatus.AUTHENTICATED, ...action.payload }
    case types.NOT_AUTHENTICATED:
      return { ...state, error: [], authStatus: userStatus.NOT_AUTHENTICATED }
    case types.USER_AUTHENTICATED_PENDING:
      return { ...state, error: [], authStatus: userStatus.PENDING }
    case types.USER_AUTHENTICATED_ERROR:
      return { ...state, authStatus: userStatus.ERROR, error: action.payload }
    default:
      return state
  }
}

// fake a call to backend server
const fakeApiCall = (time = 2000) => new Promise(resolve => setTimeout(resolve, time))

// Actions
export const userLogin = dispatch => async user => {
  dispatch({ type: types.USER_AUTHENTICATED_PENDING })
  try {
    fakeApiCall().then(() => {
      dispatch({ type: types.USER_AUTHENTICATED, payload: { email: user.email } })
    })
  } catch (error) {
    // In the realworld we'd log the error to some service like Sentury or LogRocket
    dispatch({ type: types.USER_AUTHENTICATED_ERROR, payload: { errorType: 'user authentication', error } })
  }
}

export const { Context, Provider } = createDataContext(authReducer, { userLogin }, initialState)
