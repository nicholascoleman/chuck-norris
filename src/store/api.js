import axios from 'axios'

const baseURL = 'https://api.chucknorris.io/jokes'

export const getCategories = async () => {
  const response = await axios.get(`${baseURL}/categories`)
  return response
}

export const getCategory = async category => {
  const response = await axios.get(`${baseURL}/categories=${category}`)
  return response
}

export const getCategoryJoke = async category => {
  const response = await axios.get(`${baseURL}/random?category=${category}`)
  return response
}

export const getRandomJoke = async () => {
  const response = await axios.get(`${baseURL}/random`)
  return response
}

export const searchJokes = async query => {
  const response = await axios.get(`${baseURL}/search?query=${query}`)
  return response
}
