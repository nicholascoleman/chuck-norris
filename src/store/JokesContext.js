/* eslint-disable no-console */
/* eslint-disable no-undef */
import cloneDeep from 'lodash/cloneDeep'
import moment from 'moment'
import createDataContext from 'store/createDataContext'
import * as types from 'store/types'
import * as api from 'store/api'

// #const
const initialState = {
  categories: [],
  jokes: [],
  saved: {},
  fetchingJokes: false,
  fetchingJoke: false,
  error: {},
}

export const jokesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SEARCH_JOKES: {
      if (state.saved) {
        // #map
        const jokes = action.payload.jokes.map(joke => {
          const item = joke
          if (state.saved[item.id]) {
            item.checked = true
          }
          return item
        })
        return { ...state, jokes, fetchingJokes: false, error: {} }
      }
      return { ...state, ...action.payload, fetchingJokes: false, error: {} }
    }
    case types.FETCHING_JOKES:
      return { ...state, fetchingJokes: true, error: {} }
    case types.SAVE_JOKE:
      return { ...state, saved: { ...state.saved, ...action.payload }, error: {} }
    case types.UPDATE_SAVED_JOKES: {
      const { id, checked } = action.payload
      const jokes = state.jokes.map(joke => {
        const item = joke
        if (joke.id === id) {
          item.checked = checked
        }
        return item
      })
      return { ...state, jokes }
    }
    case types.REMOVE_JOKE: {
      const newSaved = state.saved
      delete newSaved[action.payload]
      return { ...state, saved: newSaved, error: {} }
    }
    case types.FETCHING_JOKE:
      return { ...state, fetchingJoke: true, error: {} }
    case types.JOKE_ERROR:
      return { ...state, error: action.payload }
    default:
      return state
  }
}

const searchJokes = dispatch => async searchTerm => {
  try {
    dispatch({ type: types.FETCHING_JOKES })
    const response = await api.searchJokes(searchTerm)
    const result = cloneDeep(response.data.result)

    result.forEach(item => {
      const newItem = item
      newItem.truncated = true
      newItem.checked = false
      return newItem
    })
    dispatch({ type: types.SEARCH_JOKES, payload: { jokes: result } })
  } catch (error) {
    dispatch({ type: types.JOKE_ERROR, payload: { errorType: 'searchJokes', error } })
  }
}

const saveJoke = dispatch => (joke, category = 'Not Specified') => {
  try {
    const savedJoke = {}
    savedJoke.id = joke.id
    savedJoke.value = joke.value
    savedJoke.savedOn = moment().format('MMM D YYYY, hh:mm:ss')
    savedJoke.category = category
    dispatch({ type: types.SAVE_JOKE, payload: { [joke.id]: savedJoke } })
    dispatch({ type: types.UPDATE_SAVED_JOKES, payload: { id: joke.id, checked: true } })
  } catch (error) {
    dispatch({ type: types.JOKE_ERROR, payload: { errorType: 'saveJoke', error } })
  }
}

const removeJoke = dispatch => id => {
  try {
    dispatch({ type: types.REMOVE_JOKE, payload: id })
    dispatch({ type: types.UPDATE_SAVED_JOKES, payload: { id, checked: false } })
  } catch (error) {
    dispatch({ type: types.JOKE_ERROR, payload: { errorType: 'removeJoke', error } })
  }
}

const getCategories = dispatch => async () => {
  dispatch(types.FETCHING_CATEGORIES)
  try {
    const response = await api.getCategories()
    dispatch({ type: types.GET_CATEGORIES, payload: { categories: response.data, fetching: false } })
  } catch (error) {
    dispatch({ type: types.JOKE_ERROR, payload: { errorType: 'getCategories', error } })
  }
}

const getJoke = dispatch => async category => {
  try {
    dispatch({ type: types.FETCHING_JOKE, payload: { fetchingJoke: true } })
    const response = await api.getRandomJoke(category)
    dispatch({ type: types.FETCHING_JOKE, payload: { fetchingJoke: false } })
    return { joke: response.data }
  } catch (error) {
    dispatch({ type: types.JOKE_ERROR, payload: { errorType: 'getJoke', error } })
    return { joke: {} }
  }
}

export const { Context, Provider } = createDataContext(
  jokesReducer,
  { saveJoke, removeJoke, searchJokes, getJoke, getCategories },
  initialState
)
