import moxios from 'moxios'

import { getCategories, getCategory, getCategoryJoke, getRandomJoke, searchJokes } from './api'

beforeEach(() => {
  moxios.install()
})

afterEach(() => moxios.uninstall())

const baseURL = 'https://api.chucknorris.io/jokes'

test('getCategories', async () => {
  moxios.stubRequest(`${baseURL}/categories`, {
    status: 200,
    responseText: 'dog',
  })
  const response = await getCategories()
  expect(response.data).toBe('dog')
})

test('getCategory', async () => {
  const category = 'animal'
  moxios.stubRequest(`${baseURL}/categories=${category}`, {
    status: 200,
    responseText: 'dog',
  })
  const response = await getCategory(category)
  expect(response.data).toBe('dog')
})

test('getRandomJoke', async () => {
  moxios.stubRequest(`${baseURL}/random`, {
    status: 200,
    responseText: 'dog',
  })
  const response = await getRandomJoke()
  expect(response.data).toBe('dog')
})

test('searchJokes', async () => {
  const query = 'some query'
  moxios.stubRequest(`${baseURL}/search?query=${query}`, {
    status: 200,
    responseText: 'cat',
  })
  const response = await searchJokes(query)
  expect(response.data).toBe('cat')
})

test('getCategoryJoke', async () => {
  const category = 'some category'
  moxios.stubRequest(`${baseURL}/random?category=${category}`, {
    status: 200,
    responseText: 'cat',
  })
  const response = await getCategoryJoke(category)
  expect(response.data).toBe('cat')
})
