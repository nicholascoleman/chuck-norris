import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { userStatus } from 'helpers/constants'
import { Provider as AuthProvider } from 'store/AuthContext'
import { Provider as JokeProvider } from 'store/JokesContext'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import enableHooks from 'jest-react-hooks-shallow'

import Routes from './Routes'

configure({ adapter: new Adapter() })

enableHooks(jest)

const setup = () => {
  const component = shallow(
    <AuthProvider>
      <JokeProvider>
        <Router>
          <Routes />
        </Router>
      </JokeProvider>
    </AuthProvider>
  )

  return component
}

describe('<Routes />', () => {
  test('renders', async () => {
    const component = await setup(userStatus.AUTHENTICATED)
    expect(component.contains(<Routes />)).toBe(true)
  })
})
