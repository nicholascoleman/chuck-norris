import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Grid } from '@material-ui/core'
import { userStatus } from 'helpers/constants'
import { Categories, Header, Home, Jokes, Search, Login } from 'components'
import { Context as AuthContext } from 'store/AuthContext'

const authRoutes = [
  {
    path: '/categories',
    component: Categories,
  },
  {
    path: '/jokes',
    component: Jokes,
  },
  {
    path: '/search',
    component: Search,
  },
  {
    path: '/',
    component: Home,
  },
]
export const Routes = () => {
  const { state } = React.useContext(AuthContext)
  const { authStatus } = state

  // A special wrapper for <Route> that knows how to
  // handle "sub"-routes by passing them in a `routes`
  // prop to the component it renders.

  function RouteWithSubRoutes(route) {
    return (
      <Route
        path={route.path}
        render={props => (
          // pass the sub-routes down to keep nesting
          <route.component {...props} routes={route.routes} />
        )}
      />
    )
  }

  // #closure
  function renderNotAuthenticatedRoutes() {
    return (
      <Grid container item wrap="nowrap">
        <Switch>
          <Route component={Login} />
        </Switch>
      </Grid>
    )
  }

  function renderAuthenticatedRoutes() {
    return (
      <>
        <Grid item>
          <Route component={Header} />
        </Grid>
        <Grid container item wrap="nowrap">
          <Switch>
            {authRoutes.map(route => (
              <RouteWithSubRoutes key={route.path} {...route} />
            ))}
          </Switch>
        </Grid>
      </>
    )
  }

  // #routing
  return (
    <Grid data-testid="routes-container" container direction="column">
      {authStatus === userStatus.AUTHENTICATED ? renderAuthenticatedRoutes() : renderNotAuthenticatedRoutes()}
    </Grid>
  )
}

export default Routes
