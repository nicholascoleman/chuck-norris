export const userStatus = {
  AUTHENTICATED: 'AUTHENTICATED',
  PENDING: 'PENDING',
  NOT_AUTHENTICATED: 'NOT_AUTHENTICATED',
  ERROR: 'ERROR',
}
