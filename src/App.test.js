import React from 'react'
import { render } from '@testing-library/react'
import { Provider as AuthProvider } from 'store/AuthContext'
import { Provider as JokeProvider } from 'store/JokesContext'
import { userStatus } from 'helpers/constants'

import App from './App'

const setup = () => {
  const component = render(
    <AuthProvider>
      <JokeProvider>
        <App />
      </JokeProvider>
    </AuthProvider>
  )

  return component
}

describe('<App />', () => {
  test.only('renders', () => {
    const { getByTestId } = setup(userStatus.PENDING)
    expect(getByTestId('routes-container')).toBeTruthy()
  })
})
