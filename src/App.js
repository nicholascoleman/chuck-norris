import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider as AuthProvider } from 'store/AuthContext'
import { Provider as JokeProvider } from 'store/JokesContext'

import Routes from './Routes'

const App = () => {
  return (
    <AuthProvider>
      <JokeProvider>
        <Router>
          <Routes />
        </Router>
      </JokeProvider>
    </AuthProvider>
  )
}

export default App
