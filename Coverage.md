yarn run v1.22.4
$ npm test -- --coverage --watchAll=true

> chuck-norris@0.1.0 test /Users/nickcoleman/Code/chuck-norris
> react-scripts test "--coverage" "--watchAll=true"

----------------------------------|----------|----------|----------|----------|-------------------|
File                              |  % Stmts | % Branch |  % Funcs |  % Lines | Uncovered Line #s |
----------------------------------|----------|----------|----------|----------|-------------------|
All files                         |    84.48 |    78.48 |    85.54 |    84.44 |                   |
 src                              |    66.67 |       50 |    42.86 |    66.67 |                   |
  App.js                          |      100 |      100 |      100 |      100 |                   |
  Routes.js                       |       60 |       50 |    33.33 |       60 |       35,40,58,66 |
 src/components/Auth              |    86.67 |    76.67 |    83.33 |    86.21 |                   |
  Login.js                        |    85.71 |    76.67 |       80 |    85.71 |      14,16,22,131 |
  Login.styles.js                 |      100 |      100 |      100 |      100 |                   |
 src/components/Categories        |    94.29 |       90 |    93.75 |    96.77 |                   |
  Categories.js                   |    83.33 |      100 |       50 |    83.33 |                10 |
  Category.js                     |    96.55 |       90 |      100 |      100 |                67 |
 src/components/Header            |      100 |      100 |      100 |      100 |                   |
  HeaderContainer.js              |      100 |      100 |      100 |      100 |                   |
 src/components/Header/components |      100 |      100 |      100 |      100 |                   |
  Navigation.js                   |      100 |      100 |      100 |      100 |                   |
  Navigation.styles.js            |      100 |      100 |      100 |      100 |                   |
 src/components/Home              |      100 |      100 |      100 |      100 |                   |
  Home.js                         |      100 |      100 |      100 |      100 |                   |
  Home.styles.js                  |      100 |      100 |      100 |      100 |                   |
 src/components/Jokes             |    95.24 |    83.33 |      100 |    95.24 |                   |
  JokeTable.js                    |      100 |      100 |      100 |      100 |                   |
  Jokes.js                        |    88.89 |       50 |      100 |    88.89 |                26 |
 src/components/Search            |    86.05 |     87.5 |    83.33 |    89.74 |                   |
  Search.js                       |      100 |      100 |      100 |      100 |                   |
  Search.styles.js                |      100 |      100 |      100 |      100 |                   |
  SearchJoke.js                   |    94.44 |    83.33 |      100 |      100 |                19 |
  SearchResults.js                |    58.33 |      100 |       50 |       60 |       10,11,12,13 |
 src/components/Spinner           |      100 |      100 |      100 |      100 |                   |
  Spinner.js                      |      100 |      100 |      100 |      100 |                   |
 src/helpers                      |      100 |      100 |      100 |      100 |                   |
  constants.js                    |      100 |      100 |      100 |      100 |                   |
 src/store                        |    78.79 |    73.91 |    85.19 |    77.42 |                   |
  AuthContext.js                  |    84.21 |    71.43 |    83.33 |    81.25 |          22,34,38 |
  JokesContext.js                 |    65.75 |       75 |    78.57 |    63.24 |... 19,120,122,123 |
  api.js                          |      100 |      100 |      100 |      100 |                   |
  createDataContext.js            |      100 |      100 |      100 |      100 |                   |
  types.js                        |      100 |      100 |      100 |      100 |                   |
----------------------------------|----------|----------|----------|----------|-------------------|

