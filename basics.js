// Examples

import React from 'react'

const people = [
  { id: 1, fname: 'Sally', lname: 'Jones', salary: 100000 },
  { id: 2, fname: 'Joe', lname: 'Jones', salary: 75000 },
  { id: 3, fname: 'Sally', lname: 'Smith', salary: 125000 },
]

/** returns an array where every record matches the persons's
 *  @return [ { id: 1, fname: 'Sally', lname: 'Jones', salary: 100000 }, { id: 3, fname: 'Sally', lname: 'Smith', salary: 125000 }, ]
 */

// #filter
const sally = people.filter(person => person.fname === 'sally')

/**
 * return a react jsx used to render all people
 */
// #map
const renderPeople = () => {
  return people.map(person => (
    <div key={person.id}>
      <div>
        Name: {person.fname} {person.lname}
      </div>
    </div>
  ))
}

/**
 * uses the reduce function to add all the people saleries
 * @return totalSalaries = 200000
 */

const totalSalaries = people.reduce((accum, person) => {
  return accum + person.salary
}, 0)
